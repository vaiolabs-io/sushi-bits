# Counting Rice In Your Sushi

- create a variable with specific amount of rice grains in it.(depends on how many rolls you'd like to eat--> read this [link](https://www.easyhomemadesushi.com/how-much-sushi-rice-to-make-per-person/) about sushi)
- some assumption before making mess of the kitchen:
  - if you choose to make roll from the list, verify that you have enough rice grains.
  - if no or not enough rice, stop cooking(exit the script)
  - if you reach limit of rice, notify the user of the script.(limit should be 10% of amount variable set before)
  - the rice should be boiled, unless you know something about sushi i  don't... (simple functions that adds tag to rice variable, that it had been boiled)
- create a dictionary(map/hashmap/KeyValue pair) of fish you would like to have  for sushi and amount of it. e.g "salmon":5
  - if you use the fish in any of the rolls, its value in dictionary needs to be decreased.
  - if roll needs any type of vegetable, that you have not accounted for, it should be notified and also it should be a script stopper.(you need vegetable dict as well)
  - rolls also need spices and sauces.(2 more dicts)




After all that is ready, .... here is list of special **SUSHI** rolls:
- [Akatuna Roll](akatuna_roll/README.md)
- [California Roll](./california_roll/README.md)
- [Rainbow Roll](./rainbow_roll/README.md)
- [RedDragon Roll](./redgragon_roll/README.md)
- [Spicy Roll](spicy_tuna/README.md)
- [Spider Roll](./spider_roll/README.md)
- [Sun Roll](./sun_roll/README.md)
- [Unagi Nigiri](./unagi_nigiri/README.md)