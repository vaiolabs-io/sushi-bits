# Aka Tuna Roll

- To create Aka Tuna Roll you needed:
    - Tuna
    - Salmon
    - Denis
    - Caviar(salmon eggs)
    - Asparagus
    - Avocado
    - Tempura
    - Spicy mayo

- Use Python `List` to save all the ingredients.
- Print the second longest ingredient in the list.