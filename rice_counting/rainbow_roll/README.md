# Rainbow Roll

- To create Rainbow Roll you need:
        - Denis
        - Tuna
        - Red Tuna
        - Salmon
        - Avocado
        - Green onion
        - Cucumber
        - Boiled carrot

- Save the ingredients as space separated string
- Print the ingredients in reverse order
- Verify that in case of additional or too many spaces will not be included in the reverse order