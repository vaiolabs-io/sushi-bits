# Fishy Types

Welcome to basics of python type practice. 
question/tasks below are not hard and requires you to think as straight forward as possible (KISS: Keep It Simple Stupid)

---

# Numeric Types

We would like to create short shopping list that we'll use to buy fish at the market later. Please write down a script to list amount and type of fish to buy.(print them at the end of the script). if it will be too heavy to carry you should write it down.
- Salmon: 2.3 kg
- Unagi: 0.4 kg
- Tuna: 3.0 kg
- Carp: 1.8 kg
- Bass: 0.9 kg

---

# Text Types

Well, you bought the fish, might as well suggest how to prepare it: write down a recipe, using python text formatting, that will incorporate fish you bought beforehand.

---

# Sequence Types
Take the fish list and save it in appropriate sequence. Print the sequences.

---

# Mapping Types

Sometimes having list or sequence is not enough, so lets create mapping that will enable us to understand what comes in what weight.
