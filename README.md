# Sushi Bits

Project dedicated to learning to python/ruby/js/bash scripting language and sushi making... somehow.


Lets learn basics of Sushi rolling:

- Basic Ingredients
    - ### [Fish](./basics/types/README.md) <!--data types -->
    - ### [Fish_var](./basics/vars/README.md) <!-- variables -->
    - ### [Fish_operators](./basics/ops/README.md) <!-- operators -->
    - ### [Fish_conditions](./basics/cond/README.md) <!-- conditions -->
    - ### [Fish_loops](./basics/loops/README.md) <!-- loops -->
    - ### [Fish_functions](./basics/func/README.md) <!-- function -->
    - ### [Fish_Object](#) <!-- OOP -->
        - ### [Fish_class](./basics/oop/class/README.md) <!-- class -->
        - ### [Fish_attr](./basics/oop/attr/README.md) <!-- attributes -->
        - ### [Fish_methods](./basics/oop/methods/README.md) <!-- methods -->
        - ### [Fish_inherent](./basics/oop/inherent/README.md) <!-- inheritence -->
        - ### [Fish_caviates](./basics/oop/cav/README.md) <!-- caviates -->



Lets eat far east cuisine tonight:
- ### [Sushi](./rice_counting/README.md)
- ### [SaShimi](./sashimi/README.md)
- ### [Ramen](./ramen_stand/README.md)
- ### [Gyioza](./gyioza_shop/README.md)
- ### [Buns](station_ban/README.md)

